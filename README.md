# README #

This app is a short demonstration of Google's Awareness API.  Using the example of a ticket purchasing app, the final version sets up an awareness fence for the event's location and time, that is triggered when the user arrives at the venue at the right time.  Further Awareness API documentation here: https://developers.google.com/awareness/overview

### How do I get set up? ###

To run this app for yourself, you will need to create your own Awareness API key and replace the existing key in AndroidManifest with your own key.  Information on creating your own API key is found here: https://developers.google.com/awareness/android-api/get-a-key