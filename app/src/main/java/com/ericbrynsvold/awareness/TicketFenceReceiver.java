package com.ericbrynsvold.awareness;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * BroadcastReceiver which listens for changes to Location+Time Awareness Fences created when tickets are purchased.
 * The same receiver is used for all tickets, so be sure to get the specific ticket information from the Fence key.
 * The Fence key will be prefixed with {@link PurchasingActivity#TICKET_FENCE_PREFIX } and contains the ticket id after the prefix.
 */
public class TicketFenceReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        // TODO: if the triggered fence is a fence we are listening for, get fence state and act on it
    }

    private Ticket getTicketFromFenceKey(String fenceKey) {
        // for demo, always return the same ticket
        return DemoUtils.getTodayTicket();
    }
}
