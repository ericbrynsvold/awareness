package com.ericbrynsvold.awareness;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

/**
 * Activity that allows you to purchase a ticket.  After purchasing, a Location+Time Awareness API Fence will be
 * created that will be triggered one hour before the event starts, and will be live for the duration of the event.
 */
public class PurchasingActivity extends AppCompatActivity {
    private static final String TAG = PurchasingActivity.class.getSimpleName();
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 10;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchasing);

        findViewById(R.id.button_purchase).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                purchaseTicket();
            }
        });

        requestLocationPermissionIfNecessary();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "permission granted");
                } else {
                    Log.w(TAG, "permission denied");
                }
            }
        }
    }

    private void requestLocationPermissionIfNecessary() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    private void purchaseTicket() {
        // TODO: connect to GoogleApiClient and set up Fence by calling setUpTicketFence() in onConnected()

        Toast.makeText(this, "Thanks for purchasing!", Toast.LENGTH_SHORT).show();
    }

    private void setUpTicketFence() {

        final Ticket ticket = DemoUtils.getTodayTicket();

        // TODO: set up ticket fence
    }
}
