package com.ericbrynsvold.awareness;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import java.util.Calendar;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Utility methods for demonstration purposes
 */
public class DemoUtils {

    /**
     * @return a Ticket with the starting time set for today and location set for location that matches emulator (AT&T Park)
     */
    public static Ticket getTodayTicket() {
        Ticket ticket = new Ticket();

        ticket.id = "abc123";
        ticket.eventDescription = "Opening Day 2017";
        ticket.eventDuration = 3 * 60 * 60 * 1000; // 3 hrs

        Calendar eventTimeFenceStartTime = Calendar.getInstance();
        eventTimeFenceStartTime.set(2016, Calendar.NOVEMBER, 30);
        eventTimeFenceStartTime.set(Calendar.HOUR_OF_DAY, 0);
        eventTimeFenceStartTime.set(Calendar.MINUTE, 58);
        ticket.eventTime = eventTimeFenceStartTime.getTimeInMillis();

        ticket.eventLocationLatitude = 37.594077;
        ticket.eventLocationLongitude = -122.365289;

        return ticket;
    }

    // incrementing notification ID to enforce unique notifications
    private static int notificationId = 0;

    /**
     * Displays a notification with the given string as the text of the notification
     */
    public static void displayFenceActivatedNotification(Context context, String notificationText) {
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(android.R.drawable.ic_media_play)
                .setContentTitle("Fence Activated")
                .setContentText(notificationText);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(notificationId++, notificationBuilder.build());
    }
}
