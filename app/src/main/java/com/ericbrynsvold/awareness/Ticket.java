package com.ericbrynsvold.awareness;

/**
 * Model class for ticket purchasing, contains information about the ticket purchased
 */
public class Ticket {

    public String id;
    public long eventTime; // timestamp, in ms
    public long eventDuration; // in ms
    public double eventLocationLatitude;
    public double eventLocationLongitude;
    public String eventDescription;
}
